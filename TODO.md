## Backend
* Add user authentication - Only logged in user can see expense list, add/edit/delete expense. Non-registered sees a landing page.
* Add login page
* Sort the output in the template by the date added.
* Charts/Graph generation as well as analytics of data
* Export capabilities

## Frontend
* AJAXify everything. It should be a single page app
  - AJAXify Edit functionality
  - [Done] Finish abstracting form creation AJAX component
  - [Done] Clear form input(s) on submission
  - Figure out why data sent to data is an HTML output. JSONfy it.
  - [Done] AJAXify delete components
    - [Done] Remember to add confirmation dialog
  - [Done] Expense list collapsible won't click. Add JS initializer
  - Reload category select in expense form too on adding of new category [Give the category the id used presently for testing dynamic category addition]
  - [Done] Add notifications on completion/error of tasks.
    - [?]Add data-success-message which can be used to customize AJAX form submissions
  - [?]Add close modal on success of add operation
* [Done]Add confirmation to actions like delete
  - Add undo to toast that shows when item is deleted. Clcicking undo within timedelay frame stops delete action
* Customize colors
* Upgrade general look & feel
* Add padding to collapsible-body
* Color code and add icons to categories [Figure out system with Django] - Maybe use this class="ico-cat ico-cat-{{ expense.category|lowercase }}". The ico-cat adds a fallback
* Restyle forms
* Change Logo
