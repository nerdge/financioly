/*=============================================================
Non AJAX Code
- Move to appropriate location after organization
=============================================================*/

// var triggerBtn = $('#triggerBtn'); // Get ID of button that triggers open and close of Expense & Category forms

function show_category_form() {
  $("#expense-form").hide();
  // $("#category_form").show();
}

function show_expense_form(event) {
  $("#expense_form").show();
  $("#category_form").hide();
}


/*=============================================================
AJAX Code
=============================================================*/


// Abstract AJAX create form
$('.ajax-form').on('submit', function(event){
  event.preventDefault();
  var form = $(this);
  send_form(form);
});

function send_form(form) {
  $.ajax({
    url: form.attr('action'), // Get the action url from the action attribute defined on form
    type: 'post',
    data: form.serialize(),
    success: function(data) {
      var dataList = $(form.attr('data-list')); // The data-list is the ID for the list container that is reloaded upon form submission success
      var pageURL = $(location).attr('href'); // Get URL for the current page

      dataList.load(pageURL + ' ' + form.attr('data-list'), function(){
        $('.collapsible').collapsible();
      }); // Reload the associated list
      Materialize.toast('Item Added', 5000);
      form[0].reset(); // remove the value from the form inputs
      // $('.collapsible').collapsible();
    },
    failure: function(data) {
      alert('Error');
    }
  });
}

// Abstract Delete Function
$('.ajax-delete').on('click', function(event){
  event.preventDefault();
  var item = $(this);
  if (window.confirm("Do you really want to delete?")) {
    delete_item(item);
  } else {
    return false;
  }
});

function delete_item(item) {
  $.ajax({
    url: item.attr('href'), // Get delete link
    type: 'get', // Might remove since this is the default
    success: function(data) {
      console.log('Item deleted');
      $('#expense-list').load('/ #expense-list', function () {
        $('.collapsible').collapsible();
      });
      Materialize.toast('Item Deleted', 4000);
    },
    failure: function(data) {

    }
  });
}


/*=============================================================
WearProtection.js
  CSRF Protection
  Source = https://gist.github.com/broinjc/db6e0ac214c355c887e5
=============================================================*/

// This function gets cookie with a given name
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

/*
The functions below will create a header with csrftoken
*/

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});
