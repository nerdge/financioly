from django.http import *

from django.template import RequestContext

from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

from django.contrib import auth
from django.core.context_processors import csrf

from django.utils import timezone

from django.shortcuts import render, redirect, get_object_or_404, render_to_response

from .models import *
from .forms import *

# Create your views here.
@login_required
def category_new(request):
    form = CategoryForm()

    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            # return redirect('', pk=category.pk)
    else:
        form = CategoryForm()

    return render(request, 'financioly/category_form.html', {'form': form})


@login_required
def category_delete(request, pk):
    category = get_object_or_404(Category, pk=pk)
    category.delete()
    return redirect('/', pk=category.pk)


@login_required
def expense_new(request):
    form = ExpenseItemForm()

    if request.method == "POST":
        form = ExpenseItemForm(request.POST)
        if form.is_valid():
            expense = form.save(commit=False)
            expense.owner = request.user
            expense.save()
            return redirect('/', pk=expense.pk)
    else:
        form = ExpenseItemForm()

    return render(request, 'financioly/expense_form.html', {'form': form})


@login_required
def expense_list(request):
    expenses = ExpenseItem.objects.filter(created__lte=timezone.now(), owner=request.user).order_by('created')
    categories = Category.objects.all() # Add sorting. Maybe a .order-by('created'). Rememeber to add created model

    category_form = CategoryForm()
    expense_form = ExpenseItemForm()

    return render(request, 'financioly/expense_list.html', {'expenses': expenses, 'categories': categories, 'category_form': category_form, 'expense_form': expense_form})


@login_required
def expense_edit(request, pk):
    expense = get_object_or_404(ExpenseItem, pk=pk)
    if request.method == "POST":
        form = ExpenseItemForm(request.POST, instance=expense)
        if form.is_valid():
            expense = form.save(commit=False)
            expense.owner = request.user
            expense.save()
            return redirect('/', pk=expense.pk)
    else:
        form = ExpenseItemForm(instance=expense)
    return render(request, 'financioly/expense_form.html', {'form': form})


@login_required
def expense_delete(request, pk):
    expense = get_object_or_404(ExpenseItem, pk=pk)
    expense.delete()
    return redirect('/', pk=expense.pk)
