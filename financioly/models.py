from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=50, null=True)

    def __unicode__(self):
        return self.name

class ExpenseItem(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(User, related_name='expense_items')

    name = models.CharField(max_length=100, null=True)
    category = models.ForeignKey(Category, null=True, on_delete=models.SET_NULL)
    amount = models.PositiveIntegerField(null=True)
    description = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('detail', kwargs={'slug': self.slug})
